import Vue from 'vue';
import Router from 'vue-router';
import firebase from 'firebase';
import LoginPage from '@/pages/LoginPage.vue';
import HomePage from '@/pages/HomePage.vue';
import AboutPage from '@/pages/AboutPage.vue';
import AddCoursePage from '@/pages/AddCoursePage.vue';
import SignUpPage from '@/pages/SignUpPage.vue';
import RecentRoundsPage from '@/pages/RecentRoundsPage.vue';
import NewRoundPage from '@/pages/NewRoundPage.vue';
import ViewCoursePage from '@/pages/ViewCoursePage.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      redirect: '/login',
    },
    {
      path: '/',
      redirect: '/login',
    },
    {
      path: '/login',
      name: '/login',
      component: LoginPage,
    },
    {
      path: '/signup',
      name: '/signup',
      component: SignUpPage,
    },
    {
      path: '/home',
      name: 'Home',
      component: HomePage,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/about',
      name: 'about',
      component: AboutPage,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/recent',
      name: 'recent',
      component: RecentRoundsPage,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/playround',
      name: 'playround',
      component: NewRoundPage,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/addcourse',
      name: 'addcourse',
      component: AddCoursePage,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/viewcourse',
      name: 'viewcourse',
      component: ViewCoursePage,
      meta: {
        requiresAuth: true,
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) {
    next('login');
  } else if (!requiresAuth && currentUser) {
    next('home');
  } else {
    next();
  }
});

export default router;
