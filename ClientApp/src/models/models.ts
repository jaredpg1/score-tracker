export interface IRecentRounds {
  rounds?: IRound[];
}

export interface IRound {
  date_played?: Date;
  holes?: IHole[];
}

export interface IHole {
  hole_number?: number;
  hit_fairway?: boolean;
  hit_green?: boolean;
  number_putts?: number;
  score?: number;
  hole_played?: boolean;
}
