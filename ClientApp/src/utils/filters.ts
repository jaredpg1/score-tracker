import Vue from 'vue';

Vue.filter('phoneNumberFormatter', (num: string) => {
  let areaCode = '';
  let firstThree = '';
  let lastFour = '';

  if (!num) {
    return '';
  }

  areaCode = num.slice(0, 3);
  firstThree = num.slice(3, 6);
  lastFour = num.slice(6, 10);
  return '(' + areaCode + ')' + ' ' + firstThree + '-' + lastFour;
});

Vue.filter('currencyFormatter', (value: string) => {
  //  return '$' + parseFloat(value).toFixed(2);
  if (value != null) {
    return '$' + parseFloat(value).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  } else {
    return '$0.00';
  }
});

Vue.filter('dateFormatter', (value: string) => {
  const utc = (new Date(value)).toUTCString();
  return (new Date(utc).getMonth() + 1) + '/' + (new Date(utc).getDate()) + '/' + new Date(utc).getFullYear();
});
