import {
  Module,
  VuexModule,
  getModule,
  Mutation,
  Action,
} from 'vuex-module-decorators';
import store from '@/store/store';
import { ApiService } from '@/services/api.service';

@Module({
  dynamic: true,
  namespaced: true,
  name: 'home-module',
  store,
})
class HomeModule extends VuexModule {
  // @Mutation
  // REFRESH_SELECTED_BATCH_EXCEPTION_LIST(selectedBatchException: IBatchExceptionDto[]) {
  //   this.selectedBatchExceptionsList = selectedBatchException;
  // }

  // @Action({ commit: 'REFRESH_SELECTED_BATCH_EXCEPTION_LIST', rawError: true })
  // public async getBatchExceptionByExceptionId(exceptionId: string) {
  //   const selectedBatchException = await ApiService.getBatchExceptionListByExceptionId(exceptionId);
  //   return selectedBatchException;
  // }

}

export default getModule(HomeModule);
