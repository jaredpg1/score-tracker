import Vue from 'vue';
import App from '@/App.vue';
import router from '@/router/router';
import store from '@/store/store';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import firebase from 'firebase/app';
import { firestorePlugin } from 'vuefire';
import 'firebase/firestore';
import { ApiService } from '@/services/api.service';
import { StorageService } from '@/services/storage.service';
import '@/utils/filters';

Vue.config.productionTip = false;

Vue.use(Buefy);
Vue.use(firestorePlugin);

let app: any = '';
const firebaseConfig = {
  apiKey: 'AIzaSyDjXsaekmXwwz6X48jzra1jK3i1HUGJLPA',
  authDomain: process.env.AUTH_DOMAIN,
  databaseURL: process.env.DB_URL,
  projectId: 'score-tracker-c1cb6',
  storageBucket: 'score-tracker-c1cb6.appspot.com',
  messagingSenderId: '616785347888',
  appId: '1:616785347888:web:2e836aaaeeff5539',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

export const db = firebaseApp.firestore();

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: (h) => h(App),
    }).$mount('#app');
  }
});
